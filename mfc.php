<?php
/***************************************************************************************
 **********************           ЗАДАНИЕ       ****************************************
 ***************************************************************************************/
/**
 * 1) Написать функцию, которая из массива:
    $data = [
        'element1.child.property1' 	=> 1,
        'element1.child.property2' 	=> 2,
        'element2.child.name' 		=> 3,
        'element2.child2.name' 		=> 4,
        'element2.child2.position' 	=> 5,
        'element3.child3.position' 	=> 6,
    ];

    сделает:

    $result = [
    'element1' => [
        'child' => [
            'property1' => 1,
            'property2' => 2,
        ]
    ],
    'element2' => [
        'child' => [
            'name' => 3
        ],
        'child2' => [
            'name' => 4,
            'position' => 5
            ]
        ],
    'element3' => [
        'child3' => [
            'position' => 6
        ]
    ],
    ];
 * 
 */



/***************************************************************************************
 **********************            РЕШЕНИЕ      ****************************************
 ***************************************************************************************/


if (!function_exists('is_array_access')) {
    /**
     * Функция проверки возможности работы с ячейками масссива
     * @param $data
     *
     * @return bool
     */
    function is_array_access($data)
    {
        return (is_array($data) || ($data instanceof ArrayAccess));
    }
}

if (!function_exists('array_set')) {
    /**
     * Функция взята из хедперов laravel, костылить не люблю если есть готовое
     * @link http://laravel.su/docs/5.4/helpers#method-array-set
     * Там отличный набор функций для работы с массивами
     *
     * Также есть моя вариация некоторых фунций из того хелпера, использовал в файл менеджере 
     * @link https://gitlab.com/genasyst-interview/genasyst-filemanager/blob/master/temp-misc.php
     *
     * @param $array
     * @param $key
     * @param $value
     *
     * @return mixed
     */
    function array_set(&$array, $key, $value)
    {
        if (is_null($key)) return $array = $value;
        $keys = explode('.', $key);
        while (count($keys) > 1) {
            $key = array_shift($keys);
            if (!isset($array[$key]) || !is_array_access($array[$key])) {
                $array[$key] = array();
            }
            $array = &$array[$key];
        }

        $array[array_shift($keys)] = $value;

        return $array;
    }
}

if (!function_exists('array_dot')) {
    /**
     * Функция переводит массив рекурсивно в иерархический по точкам -разделителям в ключах
     *
     * @param $array
     *
     * @return array
     */
    function array_dot($array)
    {
        if (!is_array($array)) {
            return $array;
        }
        $result = array();
        foreach ($array as $k => $v) {
            array_set($result, $k, array_dot($v));
        }

        return $result;
    }
}


$data = [
    'element1.child.property1' => 1,
    'element1.child.property2' => [ /* добавил вложенный подмассив */
        'subelement1.child.property1' => 1000,
        'subelement1.child.property2' => 2000,
    ],
    'element2.child.name'      => 3,
    'element2.child2.name'     => 4,
    'element2.child2.position' => 5,
    'element3.child3.position' => 6,
];

var_export(array_dot($data));


/**
 *   RESULT
 * 
    $result = array(
        'element1' => array(
            'child' => array(
                'property1' => 1,
                'property2' =>
                    array(
                        'subelement1' =>  array(
                            'child' => array(
                                'property1' => 1000,
                                'property2' => 2000
                            ),
                        ),
                    ),
            ),
        ),
        'element2' => array(
            'child' => array(
                'name' => 3,
            ),
            'child2' => array(
                'name' => 4,
                'position' => 5,
            ),
        ),
        'element3' => array(
            'child3' => array(
                'position' => 6,
            ),
        ),
    );

 *
 */
